terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

#Create VPC
resource "aws_vpc" "appvpc" {
  cidr_block       = "${var.vpc_cidr}"
  instance_tenancy = "default"

  tags = {
    Name = "Players APP VPC"
  }
}

#Create internet gateway
resource "aws_internet_gateway" "appgateway" {
  vpc_id = "${aws_vpc.appvpc.id}"
}

#Create route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.appvpc.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.appgateway.id}"
}

#Create subnet
resource "aws_subnet" "appsubnet" {
  vpc_id                  = "${aws_vpc.appvpc.id}"
  cidr_block             = "${var.subnet_cidr}"
  map_public_ip_on_launch = true
  availability_zone = "us-east-1a"

  tags = {
    Name = "APP subnet"
  }
}

#Create and define rules for security group, the app port is 5000
resource "aws_security_group" "app_sg" {
  name        = "app_sg"
  description = "Allow custom TLS inbound traffic"
  vpc_id = "${aws_vpc.appvpc.id}"

  ingress {
    from_port        = 443
    to_port          = 443
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  ingress {
    from_port        = 5000
    to_port          = 5000
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "app_sg"
  }
}

#Create EC2 instance
resource "aws_instance" "players-app-webserver" {
  ami           = "${var.ami}"
  instance_type = "${var.instancetype}"
  key_name = "terraform-pkey"
  subnet_id = "${aws_subnet.appsubnet.id}"
  vpc_security_group_ids = ["${aws_security_group.app_sg.id}"]

  tags = {
    Name = "players-app-webserver"
  }

  #Make a SSH connection with the instance to deploy the docker image
  connection {
    host = self.public_ip
    user = "ubuntu"
    private_key = "${file("terraform-pkey.pem")}"
    type = "ssh"
  }

  #Installs docker and run the image
  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt install -y ca-certificates curl gnupg lsb-release",
      "sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg",
      "sudo echo \"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null",
      "sudo apt-get update",
      "sudo apt install docker-ce docker-ce-cli containerd.io -y",
      "sudo docker login -u ${var.registry_user} -p ${var.registry_pass}",
      "sudo docker run -dp 5000:5000 ${var.image_name}:${var.image_tag}"  
    ]
  }
}  

#Outputs server public ip and public dns after creation
output "ec2instance_ip" {
  value = aws_instance.players-app-webserver.public_ip
} 

output "ec2instance_dns" { 
  value = aws_instance.players-app-webserver.public_dns
}
# Deploy Instructions 

The infrastructure provisioning for AWS and the application deployment is made with Terraform and can be automatically executed in Gitlab CI/CD.

## How to local deploy: 

1. Create a user with "Programmatic access" and enough privileges for EC2 in AWS IAM. This processes wil generate a Key ID and Secret Key. 

2. Store the Key ID e Secret Key values as enviroment variables: 

```bash
export AWS_ACCESS_KEY_ID=value
```
```bash
export AWS_SECRET_ACCESS_KEY=value
```

3. Build and publish the docker image. Refer to README.md at the app root directory for instructions. 

4. Execute the make command at the root directory to deploy the infrastructure. 

```bash
make deploy
```

5. Get the ip or dns value returned by Terraform to access the app. http://aws_instance_ip:5000
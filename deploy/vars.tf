# Defining Region
variable "aws_region" {
  default = "us-east-1"
}

# Defining CIDR Block for VPC
variable "vpc_cidr" {
  default = "10.0.0.0/16"
}

# Defining CIDR Block for Subnet
variable "subnet_cidr" {
  default = "10.0.1.0/24"
}

# Defining AMI
variable "ami" {
  default = "ami-0e5c04126158f1889"
}


# Defining Instace Type
variable "instancetype" {
  default = "t2.micro"
}

# Defining Dockerhub user
variable "registry_user" {
    default = "gabrieru"
}

# Defining Dockerhub password
variable "registry_pass" {
    default = "6EKSUn1jYLU6rcW"
}

# Defining Docker image
variable "image_name" {
  default = "gabrieru/players-app"
}

variable "image_tag" {
  default = "1.0.0"
}
